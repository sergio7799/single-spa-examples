
// import {
//     start,
//     registerApplication
//   } from 'single-spa';
  
// //   Promise.all([
// //     System.import('pubsub-js'),
// //     System.import('snackbar')
// //   ]).then(() => {
// //     registerApplication({
// //       name: '@first/spavue2',
// //       app: () => System.import('@first/spavue2'),
// //       activeWhen: location => location.pathname.startsWith('/vue')
// //     });
// //     start();
// //   });

// registerApplication(
//   {
//     name: '@first/spavue2',
//     app: () => System.import('@first/spavue2'),
//     activeWhen: location => location.pathname.startsWith('/vue')
//   },
//   {
//     name: 'angular1',
//     app: () => System.import('angular1'),
//     activeWhen: location => location.pathname.startsWith('/angular')
//   }
// );
// start();

import {
  start,
  registerApplication
} from 'single-spa';

const apps = [
  {
    name: '@first/spavue2',
    app: () => System.import('@first/spavue2'),
    activeWhen: location => location.pathname.startsWith('/vue')
  },
  {
    name: 'angular1',
    app: () => System.import('angular1'),
    activeWhen: location => location.pathname.startsWith('/angular')
  },
  {
    name: 'react1',
    app: () => System.import('react1'),
    activeWhen: location => location.pathname.startsWith('/react')
  }
]

Promise.all([
  System.import('vue'),
  System.import('vue-router')
]).then((modules) => {
  const Vue = modules[0];
  const VueRouter = modules[1];

  Vue.use(VueRouter)
  apps.forEach(app =>  registerApplication(app) );
  start();
});