// const { merge } = require("webpack-merge");
// const singleSpaDefaults = require("webpack-config-single-spa-react");

// module.exports = (webpackConfigEnv = {}) => {
//   const defaultConfig = singleSpaDefaults({
//     orgName: "react-mf",
//     projectName: "people",
//     webpackConfigEnv,
//   });

//   const config = merge(defaultConfig, {
//     // customizations go here
//   });

//   return config;
// };


const { merge: webpackMerge } = require('webpack-merge');
const singleSpaDefaults = require('webpack-config-single-spa-react-ts');

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "single-spa-test",
    projectName: "react2",
    webpackConfigEnv,
  })

  const config = webpackMerge(defaultConfig, {
  });

  const newConfig = {
      ...config,
      devServer: {
        port: 5557,
        clientLogLevel: 'info',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
    },
    resolve: {
        extensions: ['.mjs', '.js', '.jsx', '.wasm', '.json', '.ts', '.tsx']
    }
  }

  console.log(newConfig);
//   console.log(config.module.rules[0]);

  return newConfig;
}




// const path = require('path');
// const webpack = require('webpack');
// const { CleanWebpackPlugin } = require('clean-webpack-plugin');

// module.exports = {
//     mode: 'development',
//     entry: 'src/single-spa-test-react1.tsx',
//   output: {
//     library: 'react1',
//     libraryTarget: 'system',
//     filename: 'single-spa-test-react1.tsx',
//     path: path.resolve(__dirname, 'dist'),
//     devtoolNamespace: 'react1',
//     publicPath: ''
//   },
//   module: {
//     rules: [
//         {
//           test: /\.js$/,
//           exclude: /node_modules/,
//           use: [{ loader: 'babel-loader' }]
//         }
//       ]
//   },
//   resolve: {
//     modules: [__dirname, 'node_modules'],
//     extensions: [ '.mjs', '.js', '.jsx', '.wasm', '.json', '.ts', '.tsx' ]
//   },
//   plugins: [
//     new CleanWebpackPlugin({
//       cleanAfterEveryBuildPatterns: ['dist'],
//     }),
//     new webpack.optimize.LimitChunkCountPlugin({
//       maxChunks: 1,
//     }),
//   ],
//   devtool: 'source-map',
//   externals: ['single-spa', /^@single-spa-test\//, 'react', 'react-dom'],
//   devServer: {
//     port: 5555,
//     clientLogLevel: 'info'
//   },
// };