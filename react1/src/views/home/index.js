import React from 'react'
import history from '../../helpers/history';

function Home() {
  function redirect(url) {
    console.log(history.location, 'history');
    history.push({
      pathname: history.location.pathname+url
    });
  }

  function replaceUrl(url) {
    history.replace(url);
  }

  return (
    <div>
      <button onClick={() => redirect('/about')}>Go to About Page</button>
      <br/>
      <button onClick={() => redirect('/project')}>Go to Project</button>
      {/* <br/>
      <button onClick={() => redirect('/vue')}>Go Vue</button>
      <br/>
      <button onClick={() => redirect('/angular')}>Go Angular</button> */}
      <br/>
      <button onClick={() => replaceUrl('/device/list')}>Go to devices</button>
    </div>
  );
}

export default Home;