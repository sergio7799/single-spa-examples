import React from 'react'

function Header() {

  return (
    <div className="page-title-row vc-page-title-row">
      <div className="vc-page-header">
        <div className="vc-page-header__title">
          React
        </div>
      </div>
    </div>
  );
}

export default Header;