import React, { useState } from 'react'
import { Button } from 'react-bootstrap'
import history from '../../helpers/history';
import VcModal from '../../components/VcModal';
import VcPopover from '../../components/VcPopover';
import './styles.css';
import VcCheck from '../../components/VcCheck';

function About() {
  const [show, setShow] = useState(false);

  function goBack() {
    history.goBack();
  }
  
  return (
    <div className="aboutPage">
      <div className="header">
        <button onClick={goBack}>Go Back</button>
        <label>About page!</label>
      </div>
      <div className="components">
        <div className="content">
          <Button variant="primary" onClick={()=> setShow(true)}>
            Open Modal
          </Button>
        </div>
        <div className="content">
          <VcPopover>
            <Button variant="success">Open Tooltip</Button>
          </VcPopover>
        </div>
        <div className="content">
          <VcCheck />
        </div>
      </div>
      <VcModal open={show} onClose={()=> setShow(false)} />
    </div>
  );
}

export default About;