import React from 'react'
import VcCard from '../../components/VcCard';
import './styles.css';

function Project() {

  return (
    <div className="vcProject main-content">
      {[...Array(12)].map(() => (
        <VcCard />
      ))}
    </div>
  );
}

export default Project;