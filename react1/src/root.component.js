import React, { useEffect } from 'react'
import { Route, Router, Switch } from 'react-router-dom'
import About from './views/about'
import Home from './views/home'
import Project from './views/project';
import history from './helpers/history';
// import Header from './views/header';
import './App.css';


export default function Root() {

  useEffect(() => {
    window.addEventListener('message', (event) => {
      console.log(event, 'event');
    });

    return () => {
      window.removeEventListener('message', (event) => {
        console.log(event, 'event');
      });
    }
  },[]);

  return (
    <div className="rootPage">
      <Router history={history}>
        <Switch>
          <Route
            key="device-path"
            path='/device/details/:id/react'
            exact
            component={Home}
          />
          <Route
            key="main-path"
            path='/react'
            exact
            component={Home}
          />
          <Route
            key="device-about"
            path='/device/details/:id/react/about'
            exact
            component={About}
          />
          <Route
            key="device-project"
            path='/device/details/:id/react/project'
            exact
            component={Project}
          />
          <Route
            key="main-about"
            path='/react/about'
            exact
            component={About}
          />
          <Route
            key="main-project"
            path='/react/project'
            exact
            component={Project}
          />
        </Switch>
      </Router>
    </div>
  )
}
