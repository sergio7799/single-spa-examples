import React from 'react'
import { Popover, OverlayTrigger } from 'react-bootstrap'


function VcPopover({ children }) {
  const popover = (
    <Popover id="popover-basic">
      <Popover.Title as="h3">Popover right</Popover.Title>
      <Popover.Content>
        And here's some <strong>amazing</strong> content. It's very engaging.
        right?
      </Popover.Content>
    </Popover>
  );

  return (
    <div className="VcPopover">
      <OverlayTrigger trigger="click" placement="right" overlay={popover}>
        {children}
      </OverlayTrigger>
    </div>
  );
}

export default VcPopover;