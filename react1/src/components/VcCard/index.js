import React from 'react'
import { Card, Button } from 'react-bootstrap';
import './styles.css';

function VcCard() {

  return (
    <div className="vcCard">
      <Card>
        <Card.Img variant="top" src="https://source.unsplash.com/R9OS29xJb-8" />
        <Card.Body>
          <Card.Title>Card Title</Card.Title>
          <Card.Text>
            Some quick example text to build on the card title and make up the bulk of
            the card's content.
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>
    </div>
  );
}

export default VcCard;