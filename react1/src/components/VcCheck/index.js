import React from 'react'

function VcCheck() {

  const checks = [
    {
      name: 'Check 1',
      state: true
    },
    {
      name: 'Check 2',
      state: false
    },
    {
      name: 'Check 3',
      state: false
    }
  ]

  return (
    <div className="vcCheck">
      {checks.map(check => (
        <div className="form-check">
          <input className="form-check-input" type="checkbox" id="flexCheckDefault" checked={check.state} />
          <label className="form-check-label" for="flexCheckDefault">
            {check.name}
          </label>
        </div>
      ))}
    </div>
  );
}

export default VcCheck;